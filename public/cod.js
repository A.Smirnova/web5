function onClick() {
    var U = document.getElementById("userName").value;
    var k = document.getElementById("kolvo").value;
    var c = document.getElementById("cena").value;
    if ( isNaN(c)||isNaN(k) ) alert("Введенное значение не является числом. Введите данные правильно.");
    else if (document.getElementById("userName").value == ''||document.getElementById("kolvo").value == ''||document.getElementById("cena").value == '') alert("Заполните все поля.");
    else alert(U+", стоимость вашей покупки составляет "+(c*k)+".");
}

window.addEventListener('DOMContentLoaded', function (event) {
  console.log("DOM fully loaded and parsed");
  let b = document.getElementById("my-button");
  b.addEventListener("click", onClick);
});
